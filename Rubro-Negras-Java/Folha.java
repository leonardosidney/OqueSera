class Folha {
	private int valor;
	private boolean red; //A cor do nodo ou é vermelha ou não é(preta)
	private Folha direito;
	private Folha esquerdo;
	private Folha pai; //acho que dava pra fazer sem isso...

	public Folha(int val, Folha p) {
		this.valor = val;
		this.direito = null;
		this.esquerdo = null;
		this.red = true;
		this.pai = p;
	}
	public void setValor(int val) {
		this.valor = val;
	}
	public int getValor() {
		return this.valor;
	}
	public void setCor(boolean cor) {
		this.red = cor;
	}
	public boolean getCor(){
		return red;
	}
	public void setDireito(Folha dir) {
		this.direito = dir;
	}
	public Folha getDireito() {
		return direito;
	}
	public void setEsquerdo(Folha esq) {
		this.esquerdo = esq;
	}
	public Folha getEsquerdo() {
		return this.esquerdo;
	}
	public void setPai(Folha p) {
		this.pai = p;
	}
	public Folha getPai() {
		return this.pai;
	}
}