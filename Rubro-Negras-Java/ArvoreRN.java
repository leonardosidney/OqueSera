class ArvoreRN {
	private Folha raiz;

	public ArvoreRN() {
		this.raiz = null; //Porque eu uso this? Porque é kawaii ^-^
	}

	public void AdicionaValor(int valor) {
		if(raiz == null) {
			this.raiz = new Folha(valor, null);
			this.raiz.setCor(false); //Isso deixa a raiz preta, já que a raiz sempre é preta
			return;
		}
		this.AdicionaValor(valor, raiz);//Eu crio uma função de mesma assinatura, já que eu pretendo fazer uma recursão aqui
		this.BalanceiaArvore();
	}
	private void AdicionaValor(int valor, Folha nodo) {
		if (nodo.getValor() > valor) {
			this.AdicionaEsquerdo(valor,nodo);
		} else {
			this.AdicionaDireito(valor,nodo);
		}
	}
	private void AdicionaDireito(int valor, Folha nodo){
		if (nodo.getDireito() == null) {
			nodo.setDireito(new Folha(valor,nodo));
		} else {
			this.AdicionaValor(valor,nodo.getDireito());
		}
	}
	private void AdicionaEsquerdo(int valor, Folha nodo){
		if (nodo.getEsquerdo() == null) {
			nodo.setEsquerdo(new Folha(valor,nodo));
		} else {
			this.AdicionaValor(valor,nodo.getEsquerdo());
		}
	}
	public void imprime() {
		if(raiz == null) {
			System.out.println("A arvore está vazia");
			return;
		}
		this.imprime(raiz);
	}
	private void imprime(Folha nodo) {
		if (nodo != null) {
			System.out.println(nodo.getValor());
			this.imprime(nodo.getEsquerdo());
			this.imprime(nodo.getDireito());
		}
	}
	private void BalanceiaArvore() {
		this.verificaVermelho(raiz);
	}
	private void verificaVermelho(Folha nodo) {
		if(nodo!= null) {
			if (nodo.getCor() == true) {
				if (nodo.getEsquerdo() != null && nodo.getDireito() != null) {
					if (nodo.getEsquerdo().getCor() || nodo.getDireito().getCor()) {
						System.out.println("rotacao red");
						this.rotacaoCasoDois(nodo);
					} else {
						System.out.println("rotacao black");
						this.rotacaoCasoDoisPreto(nodo);
					}
				} else if ((nodo.getEsquerdo() != null && nodo.getDireito() == null)||(nodo.getDireito() != null && nodo.getEsquerdo() == null)) {
					System.out.println("rotacao black 2");
					System.out.println(nodo.getValor());
					this.rotacaoCasoDoisPreto(nodo);
				}
			} else {
				this.verificaVermelho(nodo.getEsquerdo());
				this.verificaVermelho(nodo.getDireito());
			}
		}
	}
	private void rotacaoCasoDois(Folha nodo) {
		nodo.setCor(false); //define como preto
		if(nodo.getPai() != null) nodo.getPai().setCor(true); //define cor do avo para vermelho, se este nao for a raiz
		if(nodo.getPai().getEsquerdo() != nodo) 
			nodo.getPai().getEsquerdo().setCor(false);
		if(nodo.getPai().getDireito() != nodo)
			nodo.getPai().getDireito().setCor(false); // no caso ele procura saber qual filho é o tio, e altera a cor dele também
	}
	private void rotacaoCasoDoisPreto(Folha nodo) {
		boolean cor = false; //isso garante que, se o tio for null, não vai quebar ao tentar pegar a cor dele
		if (nodo.getPai().getDireito() != null) {
			cor = nodo.getPai().getDireito().getCor();
		}
		if (nodo.getPai().getDireito() == null  || !cor) { //se for vermelho, negado fica falso, sabemos qual o caso do tio preto
			if (nodo.getPai().getPai() != null) {
				if(nodo.getPai().getPai().getPai().getDireito() == nodo.getPai())  //obviamente estamos tratando com o filho do avo, sendo ele direito
					nodo.getPai().getPai().setDireito(nodo);
				else
					nodo.getPai().getPai().setEsquerdo(nodo);
				//com isso estão feitas as referencias do avo para o novo filho
				Folha avo = nodo.getPai().getPai(); //pegamos a referencia do avo
				nodo.getPai().setPai(nodo); //setamos o pai do filho como filho do filho =/
				nodo.setDireito(nodo.getPai()); //setamos o filho direito como antigo pai
				nodo.getDireito().setDireito(null); //tiramos a referencia do pai para o filho
				nodo.setPai(avo); //agora o avo é pai do filho e o filho é pai do pai(antigo) '-'
			} else {
				System.out.println("Aqui");
				nodo.getPai().setPai(nodo);
				nodo.setDireito(nodo.getPai()); 
				nodo.getDireito().setEsquerdo(null);
				nodo.setPai(null); //como é raiz, não tem avo
				this.raiz = nodo; // passa a ser a nova raiz
			}
			nodo.setCor(false); //trocamos as cores
			nodo.getDireito().setCor(true);
		} else { //Mesmo caso, porém agora é uma rotação a direita
			if (nodo.getPai().getPai() != null) {
				if(nodo.getPai().getPai().getPai().getDireito() == nodo.getPai())
					nodo.getPai().getPai().setDireito(nodo);
				else
					nodo.getPai().getPai().setEsquerdo(nodo);
				Folha avo = nodo.getPai().getPai(); 
				nodo.getPai().setPai(nodo); 
				nodo.setEsquerdo(nodo.getPai()); 
				nodo.getDireito().setEsquerdo(null); 
				nodo.setPai(avo); 
			} else {
				nodo.getPai().setPai(nodo);
				nodo.setEsquerdo(nodo.getPai()); 
				nodo.getEsquerdo().setDireito(null);
				nodo.setPai(null); 
				this.raiz = nodo; 
			}
			nodo.setCor(false);
			nodo.getEsquerdo().setCor(true);
		}
	}
}