typedef int tipoinfo;

struct TNodoA{
        tipoinfo info;
        struct TNodoA *esq;
        struct TNodoA *dir;
};

typedef struct TNodoA pNodoA;

pNodoA* InsereArvore(pNodoA *a, tipoinfo ch);
void preFixado(pNodoA *a);
void posFixado(pNodoA *a);
void Central(pNodoA *a);
pNodoA* consultaABP(pNodoA *a, tipoinfo chave);
pNodoA* consultaABP2(pNodoA *a, tipoinfo chave);
pNodoA * exclui(pNodoA *raiz, int valor); //Não me orgulho disso
pNodoA * impossivel_nao_fazer_gambiarra(pNodoA* raiz, int menor, int maior);
