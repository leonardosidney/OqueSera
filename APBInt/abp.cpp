#include <stdio.h>
#include <stdlib.h>
#include "abp.h"



pNodoA* InsereArvore(pNodoA *a, tipoinfo ch){
	 if (a == NULL){
		 a =  (pNodoA*) malloc(sizeof(pNodoA));
		 a->info = ch;
		 a->esq = NULL;
		 a->dir = NULL;
		 return a;
	 }
	 else
		  if (ch < a->info)
			  a->esq = InsereArvore(a->esq,ch);
		  else if (ch > a->info)
			  a->dir = InsereArvore(a->dir,ch);
	 return a;
}

void preFixado(pNodoA *a){
	 if (a!= NULL){
		  printf("%d\n",a->info);
		  preFixado(a->esq);
		  preFixado(a->dir);
	  }
}

void Central(pNodoA *a){
	 if (a!= NULL){
		  Central(a->esq);
		  printf("%d\n",a->info);
		  Central(a->dir);
	  }
}

void posFixado(pNodoA *a)
{
	 if (a!= NULL)
	 {
		  posFixado(a->esq);
		  posFixado(a->dir);
		  printf("%d\n",a->info);
	  }
}

pNodoA* consultaABP(pNodoA *a, tipoinfo chave) {

	while (a!=NULL){
		  if (a->info == chave )
			 return a; //achou ent�o retorna o ponteiro para o nodo
		  else
			if (a->info > chave)
			   a = a->esq;
			else
			   a = a->dir;
			}
			return NULL; //se n�o achou
}

pNodoA* consultaABP2(pNodoA *a, tipoinfo chave) {
	if (a!=NULL) {


	   if (a->info == chave)
		 return a;
	   else
		   if (a->info > chave)
				return consultaABP2(a->esq,chave);
			if (a->info < chave)
				return consultaABP2(a->dir,chave);
			else return a;
	   }
	   else return NULL;
}

pNodoA * exclui(pNodoA *raiz, int valor) {//Eu deveria ter vergonha...
	//olhar pelo lado oposto da recursao, na volta e n�o da ida
	//o return retorna o filho para o pai
	pNodoA *filho = NULL;
	if (raiz->info == valor) {
		return raiz;
	} else if (raiz->info > valor) {
		filho = exclui(raiz->esq, valor);
	} else {
		filho = exclui(raiz->dir, valor);
	}
    if(filho != NULL) {
        if(filho->dir == NULL && filho->esq==NULL) {
            if(filho ==raiz->dir) {
                raiz->dir=NULL;
            } else {
                raiz->esq = NULL;
            }
            free(filho);
        }else if((filho->dir!=NULL && filho->esq==NULL)||(filho->dir==NULL && filho->esq!=NULL)) {
            if(filho->dir!=NULL) {
                if(raiz->esq == filho)
                    raiz->esq = filho->dir;
                else
                    raiz->dir = filho->dir;
            } else {
                if(raiz->esq != filho)
                    raiz->esq = filho->esq;
                else
                    raiz->dir = filho->esq;
                }
            free(filho);
        } else if(filho->dir !=NULL && filho->esq != NULL) {
            if(raiz->dir==filho) {
                raiz->dir = impossivel_nao_fazer_gambiarra(filho,filho->esq->info,filho->dir->info);
                raiz->dir->dir = filho->dir;
                raiz->dir->esq = filho->esq;
            } else {
                raiz->esq =  impossivel_nao_fazer_gambiarra(filho,filho->esq->info,filho->dir->info);
                raiz->esq->dir = filho->dir;
                raiz->esq->esq = filho->esq;
            }
            free(filho);
        }
	}
	return NULL;
}
pNodoA * impossivel_nao_fazer_gambiarra(pNodoA* raiz, int menor, int maior) {
    pNodoA * filho = NULL;
    if(raiz == NULL) return NULL;
    if(raiz->esq ==NULL && raiz->dir == NULL && raiz->info > menor && raiz->info < maior) {
        return raiz;
    }
    if(filho==NULL)
        filho = impossivel_nao_fazer_gambiarra(raiz->esq,menor,maior);
    if (filho == NULL)
        filho = impossivel_nao_fazer_gambiarra(raiz->dir,menor,maior);
    if(raiz->dir == filho)
        raiz->dir = NULL;
    else if(raiz->esq == filho)
        raiz->esq == NULL;
    return filho;
}
