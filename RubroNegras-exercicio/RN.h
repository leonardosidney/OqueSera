#include <stdio.h>
#include <stdlib.h>

typedef struct  RNnodo RNtree;

struct RNnodo {
  int key;
  int red; /* se red=0 ent�o o nodo � preto */
  struct RNnodo* esq;
  struct RNnodo* dir;
  struct RNnodo* pai;
};

// declarar o nodo NULL
static RNtree* NodoNULL = NULL;



// definicos das Funcoes
RNtree* Insere(RNtree* t, int key);
int Consulta(int X, RNtree* T );
RNtree* Remove(RNtree* t, int key);
void Destroi(RNtree* t);

// con��es Auxiliares
RNtree* VerificaRN(RNtree* t, int key);
RNtree* RotacaoSimplesEsq(RNtree* t);
RNtree* RotacaoSimplesDir(RNtree* t);
void Desenha(RNtree* t , int nivel);
RNtree* Maior(RNtree* t);
RNtree* Menor(RNtree* t);



RNtree *  avo(RNtree *);
RNtree *  tio(RNtree *);
RNtree *  insercao(RNtree *, int);
RNtree *  verificarNodos(RNtree *, RNtree *, int);
RNtree *  tioVermelho(RNtree *, int);
RNtree *  tioPretoDupla(RNtree *, int);
RNtree *  tioPretoSimples(RNtree *, RNtree *, int);
RNtree * buscaValor(RNtree *, int);
