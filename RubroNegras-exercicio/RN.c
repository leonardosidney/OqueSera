#include <stdio.h>
#include <stdlib.h>
#include "RN.h"

RNtree* Insere(RNtree* t, int key)
{
            RNtree* x = t;

            if(t == NULL)  {
                 // aloca��o de espa�o
                 NodoNULL = (RNtree*) malloc(sizeof(RNtree));
                 x = (RNtree*) malloc(sizeof(RNtree));
                 //inicializa��es
                 // NULL
                 NodoNULL->red = 0; // null � preto;
                 NodoNULL->esq = NodoNULL;
                 NodoNULL->dir = NodoNULL;
                 NodoNULL->key = 32000;
                 NodoNULL->pai = NodoNULL;
                 // Raiz
                 x->key = key;
                 x->esq = x->dir = NodoNULL;
                 x->red = 0;
                 x->pai = NodoNULL;
                 return x;
            }
            RNtree* p= x->pai; // pai
            RNtree* v= p->pai; // vo


            while( x != NodoNULL )  /* desce na �rvore */
            {
              v = p; p = x;
              if( key < x->key ) x = x->esq;
              else x = x->dir;
            };

            if(x != NodoNULL) return t; // Nodo Ja Existe

            x = (RNtree*) malloc(sizeof(RNtree));
            x->key = key;
            x->esq = x->dir = NodoNULL;
            x->pai = p;
            x->red = 1;

            if( key < p->key)
                p->esq = x;
            else
                p->dir = x;

            // Nodo Foi Inserido mas pode ter modificado as regras ent�o temos que verificar.
            //  insercao_caso1(t, key);
             return p;
};

int Consulta(int X, RNtree* T )
{
  if( T == NodoNULL ) return 0;
  if(X == T->key) return 1;
  else if( X < T->key ) return Consulta( X, T->esq );
       else if( X > T->key ) return Consulta( X, T->dir );
            else return 0;
};


RNtree* Remove(RNtree* t, int key)
{
  RNtree* x = t;
  RNtree* y;
  RNtree* p = x->pai;
  RNtree* v = p->pai;

  NodoNULL->key = key;
  while(x->key != key)  /* desce na �rvore */
  {
   v = p; p = x;
   if( key < x->key ) x = x->esq;
   else x = x->dir;
  };
  if(x == NodoNULL) return t;
  if(x->red) // nodo � vermelho
  {
    if((x->esq == NodoNULL) && (x->dir == NodoNULL)) // nodo folha
    {
      if(x->key < p->key) p->esq = NodoNULL;
      else p->dir = NodoNULL;
      free(x);
      return t;
    };
    if(x->key < p->key)
    {
     y = Menor(t->dir);
     p->esq = y;
     y->esq = x->esq;
     y->dir = x->dir;
     free(x);
    } else
      {
        y = Maior(t->esq);
        p->dir = y;
        y->dir = x->dir;
        y->esq = x->esq;
        free(x);
      };
  }
  else // nodo � preto
  {
    if(x->key < p->key) // filho a esquerda
    {
      if((p->dir->red == 0) && ((x->esq->red == 0) && (x->dir->red == 0))) // irm�o preto e dois filhos pretos
      {
        if(p->red) p->red = 0; // troca a cor do pai
        else p->red = 1;
        p->dir->red = 1; // troca a cor do irmao
        y = Menor(t->dir);
        p->esq = y;
        y->esq = x->esq;
        y->dir = x->dir;
        free(x);
      } else if ((p->dir->red == 0) && ((x->esq->red != 0) || (x->dir->red != 0)))
        {
         y = Menor(t->dir);
         p->esq = y;
         y->esq = x->esq;
         y->dir = x->dir;
         RotacaoSimplesDir(p);
         if(x->red) x->red = 0; // troca a cor do nodo
         else x->red = 1;
         free(x);
        }else // irm�o vermelho
          {
            if(p->dir->red)
            {
               y = Menor(t->dir);
               p->esq = y;
               y->esq = x->esq;
               y->dir = x->dir;
               RotacaoSimplesDir(p);
               free(x);
            };
          };
    } else // filho a direita
      {
      if((p->esq->red == 0) && ((x->esq->red == 0) && (x->dir->red == 0))) // irm�o preto e dois filhos pretos
      {
        if(p->red) p->red = 0; // troca a cor do pai
        else p->red = 1;
        p->dir->red = 1; // troca a cor do irmao
        y = Maior(t->esq);
        p->dir = y;
        y->dir = x->dir;
        y->esq = x->esq;
        free(x);
      } else if ((p->dir->red == 0) && ((x->esq->red != 0) || (x->dir->red != 0)))
        {
         y = Maior(t->esq);
        p->dir = y;
        y->dir = x->dir;
        y->esq = x->esq;
         RotacaoSimplesEsq(p);
         if(x->red) x->red = 0; // troca a cor do nodo
         else x->red = 1;
         free(x);
        };
    };
  };
  VerificaRN(t,key);
  return t;
};

void Destroi(RNtree* t)
{

};

RNtree* VerificaRN(RNtree* t,int key)
{
  RNtree* x = t;
  RNtree* p = x->pai;
  RNtree* v = p->pai;
  while( x->key != key )  /* desce na �rvore */
  {
    v = p; p = x;
    if( key < x->key ) x = x->esq;
    else x = x->dir;
  };
  // x continuam o novo e p o pai do novo.

  /* if(p->red == 0) - caso 1
     insere vermelho mas j� t�m ent�o n�o precisa modificar.
  */
  //
  if(p->red)
  {
    if( v != NodoNULL) // pai nao � raiz
    {
      if( p-> key < v->key) // p � filho a esquerda
      {
        //
        if(v->dir->red) // tio � vermelho
        {
          v->dir->red =0; // tio vira preto
          if(p->red) p->red = 0; // troca a cor do pai
          else p->red = 1;
          if(v->pai == NodoNULL) // arvore � raiz
          {
            p->dir->red = 0;
            p->red = 0;
            v->red = 0;
          };
        } else
          {
            if((x->key < p->key) && (p->key < v->key))
            {
              // rotacao a direita
              RotacaoSimplesDir(v);
              if(p->red) p->red = 0; // troca a cor do pai
              else p->red = 1;
              if(v->red) v->red = 0; // troca a cor do vo
              else v->red = 1;
            } else
              {

              };
          };
      } else
        {
        //
        if(v->esq->red) // tio � vermelho
        {
          v->esq->red =0; // tio vira preto
          if(p->red) p->red = 0; // troca a cor do pai
          else p->red = 1;
          if(v->pai == NodoNULL) // arvore � raiz
          {
            p->dir->red = 0;
            p->red = 0;
            v->red = 0;
          };
        };
      };
    };
  };
  t->red = 0;
  x->red = 1;
  return t;
};

RNtree* RotacaoSimplesDir( RNtree* t )
        {
            RNtree* aux;
            aux = t->esq;
            t->esq = aux->dir;
            aux->dir = t;
            printf("<<>>>%d\n", aux->pai->pai->key);
            return aux;  /* nova raiz */
        };


RNtree* RotacaoSimplesEsq( RNtree* t )
        {
            RNtree* aux;
            aux = t->dir;
            t->dir = aux->esq;
            aux->esq = t;
            return aux;   /* nova raiz */
        };

void Desenha(RNtree* t , int nivel)
{
int x;

 if (t !=NodoNULL)
 {
   for (x=1; x<=nivel; x++)
      printf("=");
   if(t->red) printf("%d Red\n", t->key);
   else printf("%d Black\n", t->key);
   if (t->esq != NodoNULL) Desenha(t->esq, (nivel+1));
   if (t->dir != NodoNULL) Desenha(t->dir, (nivel+1));
   Destroi(t);
 }
 else printf("Arvore Vazia\n");
};


RNtree* Maior(RNtree* t)
{
 while (t != NodoNULL) t = t->dir;
 return t->pai;
};


RNtree* Menor(RNtree* t)
{
  while (t != NodoNULL) t = t->esq;
  return t->pai;
};




RNtree * avo(RNtree *n)
{
	if ((n != NULL) && (n->pai != NULL))
		return n->pai->pai;
	else
		return NULL;
}

RNtree * tio(RNtree *n)
{
	RNtree *g = avo(n);
	if (g == NULL)
		return NULL; // nao ter vai significa nao ter tio
	if (n->pai == g->esq)
		return g->dir;
	else
		return g->esq;
}

RNtree * buscaValor(RNtree * arvore, int key){
    if(arvore == NULL) return arvore;
    if(arvore->key < key) {
		if(arvore->dir != NULL) {
			arvore = buscaValor(arvore->dir, key);
		} else {
			return arvore;
		}
	} else {
		if(arvore->esq != NULL) {
			arvore = buscaValor(arvore->esq, key);
		} else {
			return arvore;
		}
	}
    return arvore;
}

RNtree * insercao(RNtree * arvore, int key){
    RNtree * arvoreAux = arvore;
    // printf("ola:%p\nkey1:%d\n", arvoreAux, key);
    if(arvoreAux == NULL){
        arvoreAux = (RNtree *) malloc(sizeof(RNtree));
        arvoreAux->pai = NULL;
        arvoreAux->red = 0;
        arvoreAux->key = key;
        arvoreAux->esq = arvoreAux->dir = NULL;

        return arvoreAux;
    }else{

        arvoreAux = buscaValor(arvoreAux, key);
        RNtree * arvoreFilho = (RNtree *) malloc(sizeof(RNtree));
        arvoreFilho->key = key;
        arvoreFilho->red = 1;
        arvoreFilho->pai = arvoreAux;
        arvoreFilho->esq = arvoreFilho->dir = NULL;

        // printf("%d\nkey:%d\n", arvoreFilho->key, key);
        if(arvoreAux->key < key){
            arvoreAux->dir = arvoreFilho;
            // envia o elemento atualmente inserido
            return verificarNodos(arvoreAux, arvoreAux->dir, key);
        }else{
            arvoreAux->esq = arvoreFilho;
            return verificarNodos(arvoreAux, arvoreAux->esq, key);
        }
    }
}

RNtree * verificarNodos(RNtree * arvore, RNtree * arvoreAux, int key){

	if (arvoreAux->pai->red == 0){
        RNtree * arvoreAuxAux = arvoreAux;
        while (arvoreAuxAux->pai != NULL) {
            arvoreAuxAux = arvoreAux->pai;
        }
        return arvoreAuxAux;
    }
	else{
        return tioVermelho(arvoreAux, key);
    }
    return NULL;
}


RNtree * tioVermelho(RNtree * arvoreAux, int key){
	RNtree * tioNodo = tio(arvoreAux), * avoNodo;

	if ((tioNodo != NULL) && (tioNodo->red == 1)) {
		arvoreAux->pai->red = 0;
		tioNodo->red = 0;
		avoNodo = avo(arvoreAux);

        if(avoNodo->pai == NULL){
            // printf("%d\n", avoNodo->dir->dir->esq->key)
            avoNodo->red = 0;
            return avoNodo;
        }else{
            avoNodo->red = 1;
            verificarNodos(arvoreAux, avoNodo, key);
        }

	} else {
        // printf("key:%d\n%p\n", key, tioNodo);
		return tioPretoDupla(arvoreAux, key);
	}
    return NULL;
}

RNtree * tioPretoDupla(RNtree * arvoreAux, int key)
{
	RNtree * avoNodo = avo(arvoreAux);
    RNtree * maisAux;
    // printf("pai:%p\n", arvoreAux->pai);
    // printf("filho:%p\n", arvoreAux->pai->esq);
    // printf("filho:%p\n", arvoreAux);
    // printf("pai:%p\n", avoNodo->dir);

	if ((arvoreAux == arvoreAux->pai->dir) && (arvoreAux->pai == avoNodo->esq)) {
		maisAux = RotacaoSimplesEsq(arvoreAux->pai);
		arvoreAux = arvoreAux->esq;
	} else if ((arvoreAux == arvoreAux->pai->esq) && (arvoreAux->pai == avoNodo->dir)) {
        printf("--->%d\n", arvoreAux->pai->key);
		maisAux = RotacaoSimplesDir(arvoreAux->pai);
		arvoreAux = arvoreAux->dir;
	}

	return tioPretoSimples(arvoreAux, maisAux, key);
}

RNtree * tioPretoSimples(RNtree * arvoreAux, RNtree * maisAux, int key)
{
	RNtree * avoNodo = avo(arvoreAux);

	arvoreAux->pai->red = 0;
	avoNodo->red = 1;
	if ((arvoreAux == arvoreAux->pai->esq) && (arvoreAux->pai == avoNodo->esq)) {
		maisAux = RotacaoSimplesDir(avoNodo);
	} else {
		/*
		 * Aqui, (n == n->pai->dir) && (n->pai == g->dir).
		 */
        printf("%d\n", arvoreAux->pai->key);
		maisAux = RotacaoSimplesEsq(avoNodo);
	}

    return maisAux;
}
