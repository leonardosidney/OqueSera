class Arvore {
	Nodo raiz;
	public Arvore() {
		raiz = null;
	}
	public void AdicionaValor(int valor) {
		if(raiz == null)
			this.AdicionaRaiz(valor);
		else {
			this.AdicionaNodo(valor,raiz);
		}
	}
	private void AdicionaRaiz(int valor) {
		raiz = new Nodo(valor);
	}
	private void AdicionaNodo(int valor, Nodo nodo){
		if(nodo != null) {
			if(valor < nodo.getValor())
				this.InsereEsquerda(valor,nodo);
			else
				this.InsereDireita(valor,nodo);
			this.Balanceia();
		}
	}
	private void InsereEsquerda(int valor, Nodo nodo) {
		if(nodo.getEsquerdo() != null) {
			this.AdicionaNodo(valor,nodo.getEsquerdo());
		} else {
			nodo.setEsquerdo(new Nodo(valor));
		}
	}
	private void InsereDireita(int valor, Nodo nodo) {
		if(nodo.getDireito() != null) {
			this.AdicionaNodo(valor,nodo.getDireito());
		}else {
			nodo.setDireito(new Nodo(valor));
		}
	}
	public void Imprime() {
		if(raiz == null) {
			System.out.println("Arvore vazia");
			return;
		}
		this.Imprime(raiz);
	}
	private void Imprime(Nodo nodo) {
		if(nodo != null) {
			System.out.println(nodo.getValor());
			System.out.println("Fator " + nodo.getFator());
			this.Imprime(nodo.getEsquerdo());
			this.Imprime(nodo.getDireito());
		}
	}
	private void Balanceia() {
		//this.AtualizaFb();
		this.Balanceia(raiz);
	}
	private void Balanceia(Nodo nodo) {
		if(nodo == null) return;
		nodo.setFator(this.AtualizaAltura(nodo.getEsquerdo()) - this.AtualizaAltura(nodo.getDireito()));
		this.Balanceia(nodo.getEsquerdo());
		this.Balanceia(nodo.getDireito());
	}

	private int AtualizaAltura(Nodo nodo) {
		int alt_esq, alt_dir;
		if(nodo == null) return 0;
		else {
			alt_esq = this.AtualizaAltura(nodo.getEsquerdo());
			alt_dir = this.AtualizaAltura(nodo.getDireito());
			if (alt_esq > alt_dir) {
				return (1 + alt_esq);
			} else {
				return (1+ alt_dir);
			}
		}
	}
}
