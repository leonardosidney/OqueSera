class Nodo {
    private Nodo direito;
    private Nodo esquerdo;
    private int valor;
    private int fator;
    public Nodo(int val, Nodo esq, Nodo dir) {
    	this.valor = val;
    	this.esquerdo = esq;
    	this.direito = dir;
    	this.fator = 0;
    }
    public Nodo(int obj) {
    	this.valor = obj;
		this.esquerdo = null;
		this.direito = null;
		this.fator = 0;
	}
	public void setDireito(Nodo dir) {
		this.direito = dir;
	}
	public Nodo getDireito() {
		return this.direito;
	}
	public void setEsquerdo(Nodo esq) {
		this.esquerdo = esq;
	}
	public Nodo getEsquerdo() {
		return this.esquerdo;
	}
	public void setValor(int val) {
		this.valor = val;
	}
	public int getValor() {
		return this.valor;
	}
	public void setFator(int fat) {
		this.fator = fat;
	}
	public int getFator() {
		return this.fator;
	}
}
